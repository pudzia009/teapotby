<?php
// Heading 
$_['heading_title']    = 'أحدث بلوق';
$_['heading_description'] = 'كيف كانت كل هذه الفكرة الخاطئة من شجب اللذة والألم مشيدا أشرح لك حساب كامل للنظام، وهذه الأعراض.';

// Text
$_['text_read_more']   = 'اقرأ أكثر';
$_['text_date_added']  = 'تم إضافة التاريخ:';
$_['entry_comment']       = 'تعليقات';

// Button
$_['button_all_blogs'] = 'انظر جميع المدونات';

// Heading_text
$_['heading_text']      = 'معلومات عنا';