<?php
// Heading
$_['heading_title']     = 'النشرات الإخبارية';

// Entry
$_['entry_email']          = 'أدخل بريدك الإلكتروني';

// Email
$_['email_subject']       = 'أهلا بك';
$_['email_content']       = 'شكرا لك على الاشتراك';

// Button
$_['email_button']       = 'الاشتراك';

//Description
$_['heading_desc']     = 'الاشتراك! عناصر جديدة.';