<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com
$_['cart_title']  = 'عربة التسوق';
// Text
$_['text_items']    = '%s';
$_['text_empty']    = 'سلة الشراء فارغة !';
$_['text_cart']     = 'معاينة السلة';
$_['text_checkout'] = 'إنهاء الطلب';
$_['text_recurring']  = 'ملف الدفع';
$_['headingtitle']    = 'عربة التسوق';