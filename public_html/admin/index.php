<?php

// Version
define('VERSION', '3.0.3.8');

// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

if (!IS_PRODUCTION) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: ../install/index.php');
	exit;
}

require_once __DIR__ . '/../system/storage/vendor/autoload.php';

// Startup
require_once(DIR_SYSTEM . 'startup.php');

start('admin');