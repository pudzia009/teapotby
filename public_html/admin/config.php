<?php

define('DIR_ADMIN', __DIR__ . '/');
define('DIR_PUBLIC', dirname(__DIR__) . '/');

$localConfig = DIR_PUBLIC . '/config-local.php';

if (file_exists($localConfig)) {
    require_once $localConfig;
}

// HTTP
define('HTTP_SERVER', DOMAIN_MAIN . '/admin/');
define('HTTP_CATALOG', DOMAIN_MAIN);

// HTTPS
define('HTTPS_SERVER', HTTP_SERVER);
define('HTTPS_CATALOG', HTTP_CATALOG);

// CUSTOM DIR
define('DIR_UPPER', dirname(DIR_PUBLIC) . '/');

// DIR
define('DIR_APPLICATION', DIR_ADMIN . '/');
define('DIR_SYSTEM', DIR_PUBLIC . 'system/');
define('DIR_IMAGE', DIR_PUBLIC . 'image/');
define('DIR_STORAGE', DIR_UPPER . 'storage/');
define('DIR_CATALOG', DIR_PUBLIC . 'catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');


// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
